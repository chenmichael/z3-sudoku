﻿using Microsoft.Z3;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Z3Test
{
    class Program
    {
        private readonly Context ctx;
        private readonly Solver solver;

        public IntExpr Cell(int row, int col) => ctx.MkIntConst($"cell_{row + 1}_{col + 1}");
        public IntExpr Cell((int row, int col) cell) => Cell(cell.row, cell.col);

        public BoolExpr CellDigits
            => ctx.MkAnd(AllCells.SelectMany(cell => new BoolExpr[] { cell > 0, cell < 10 }));

        public BoolExpr GroupConstraints
            => ctx.MkAnd(AllGroups.SelectMany(group => new BoolExpr[] { ctx.MkDistinct(group.ToArray()), ctx.MkEq(ctx.MkAdd(group), ctx.MkInt(45)) }));

        public IEnumerable<IntExpr> AllCells
            => Enumerable.Range(0, 9).SelectMany(Row);

        public IEnumerable<IntExpr> Row(int row)
            => Enumerable.Range(0, 9).Select(col => Cell(row, col));

        public IEnumerable<IntExpr> Col(int col)
            => Enumerable.Range(0, 9).Select(row => Cell(row, col));

        public IEnumerable<IEnumerable<IntExpr>> AllGroups
            => AllSubGrids.Concat(AllRows).Concat(AllCols);

        public IEnumerable<IEnumerable<IntExpr>> AllSubGrids
            => Enumerable.Range(0, 3).SelectMany(x => Enumerable.Range(0, 3).Select(y => SubGrid(x, y)));

        public IEnumerable<IEnumerable<IntExpr>> AllRows
            => Enumerable.Range(0, 9).Select(Row);

        public IEnumerable<IEnumerable<IntExpr>> AllCols
            => Enumerable.Range(0, 9).Select(Col);

        public IEnumerable<IntExpr> SubGrid(int x, int y)
            => Enumerable.Range(0, 3).SelectMany(dx => Enumerable.Range(0, 3).Select(dy => Cell(x * 3 + dx, y * 3 + dy)));

        public BoolExpr FillCells(Sudoku input)
            => ctx.MkAnd(input.FilledCells.Select(FillField));

        public BoolExpr FillField(((int row, int col) cell, int digit) field) => ctx.MkEq(Cell(field.cell), ctx.MkInt(field.digit));

        public Program(Context ctx)
        {
            this.ctx = ctx;
            solver = ctx.MkSolver("QF_LIA");

            solver.Assert(CellDigits);
            solver.Assert(GroupConstraints);
        }

        public Sudoku Solve(Sudoku input)
        {
            var status = solver.Check(FillCells(input));
            if (status is Status.UNSATISFIABLE) throw new ArgumentException("Input sudoku has no solution!");
            if (status is Status.UNKNOWN) throw new ArgumentException("Input sudoku cannot be solved with this solver!");
            Contract.Assert(status is Status.SATISFIABLE);
            return Sudoku.FromString(ModelToString(solver.Model));
        }

        private string ModelToString(Model model)
            => new string(Enumerable.Range(0, 9).SelectMany(row => Enumerable.Range(0, 9).Select(col => ReadModelCell(model, Cell(row, col)).ToString()[0])).ToArray());

        public int ReadModelCell(Model model, IntExpr cell)
        {
            var value = model.Eval(cell);
            if (!(value is IntNum digit)) throw new InvalidProgramException($"Expression in cell {cell} cannot be evaluated: {value}!");
            return digit.Int;
        }

        static void Main(string[] args)
        {
            using var ctx = new Context();
            var solver = new Program(ctx);
            var solved = solver.Solve(Sudoku.FromString("4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"));
            Console.WriteLine("Solution: {0}", solved.ToString());
        }
    }
}