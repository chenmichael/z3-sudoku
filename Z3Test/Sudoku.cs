﻿using Microsoft.Z3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Z3Test
{
    public class Sudoku
    {
        private const char EMPTYCELL_CHAR = '.';
        private const int SUDOKUCELL_COUNT = 9 * 9;
        private readonly int?[,] Grid = new int?[9, 9];
        public int? this[int row, int col]
        {
            get => Grid[row, col];
            private set => Grid[row, col] = value;
        }
        public IEnumerable<((int row, int col) cell, int digit)> FilledCells
            => Enumerable.Range(0, 9).SelectMany(row => Enumerable.Range(0, 9).Select(col => (row, col)))
            .Where(cell => Grid[cell.row, cell.col].HasValue).Select(cell => (cell, Grid[cell.row, cell.col]!.Value));

        public static Sudoku FromString(string input)
        {
            if (input.Length != SUDOKUCELL_COUNT) throw new ArgumentException($"Invalid sudoku size: {input.Length}!", nameof(input));
            var sudoku = new Sudoku();
            for (var row = 0; row < 9; row++)
                for (var col = 0; col < 9; col++)
                {
                    var val = input[col + 9 * row];
                    if (val is EMPTYCELL_CHAR) continue;
                    var digit = int.TryParse(val.ToString(), out var parsed) ? (parsed == 0 ? null : (int?)parsed) : null;
                    if (digit is null) throw new ArgumentException($"Position {col + 9 * row} contains invalid digit {val}: {input.Length}!", nameof(input));
                    sudoku[row, col] = digit;
                }
            return sudoku;
        }
        public override string ToString()
        {
            var sb = new StringBuilder(100);
            for (var row = 0; row < 9; row++)
                for (var col = 0; col < 9; col++)
                {
                    var cell = Grid[row, col];
                    sb.Append(cell is null ? EMPTYCELL_CHAR : cell!);
                }
            var sudokustring = sb.ToString();
            if (sudokustring.Length != SUDOKUCELL_COUNT) throw new InvalidProgramException($"Sudoku string of length {SUDOKUCELL_COUNT} expected!");
            return sudokustring;
        }
    }
}